#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

//Cette fonction retourne la longeur de la chaine sans compter le caractère de fin de chaine
int ft_strlen(char *c)
{
    //compte le nombre d'élément d'une chaine de caractère mais sans le caractère de fin
    int i;

    i = 0;
    while (*c != '\0')
    {
        i++;
        c++;
    }
    return (i);
}

int ft_is_str_valid(char* c, int n)
{
	while (*c != '\0')
    {
		if (*c < '0' || *c > n + '0')
			return (0);
		++c;
		if (*c == ' ')
			++c;
		else if (*c != '\0')
			return (0);
    }
    return (1);
}

int ft_nbrchar(char *c, char search)
{
	int i;

	i = 0;
	while (*c != '\0')
    {
		if (*c == search)
			i++;
		++c;
    }
    return (i);
	
}

int ft_check_pair(char *c, int n)
{
	int i;
	int mid;
	char x;
	char y;

	if (n == 1)
	{
		if (c[0] != '1' && c[2] != '1' && c[4] != '1' && c[6] != '1')
			return (0);
		return (1);
	}
	mid = (n + 1) / 2;
	i = 0;
	while (i < n)
    {
		// chech pour ( up, down)
		x = c[2*i];
		y = c[2*(n + i)];
		if ((x == '1' && y == '1') || (x > mid + '0' && y > mid + '0') || (x == n - '0' && y != '1') || (y == n - '0' && x != '1'))
		{
			return (0);
		}
		// chech pour ( left, right)
		x = c[4*n + 2*i];
		y = c[6*n + 2*i];
		if ((x == '1' && y == '1') || (x > mid + '0' && y > mid + '0') || (x == n - '0' && y != '1') || (y == n - '0' && x != '1'))
			return (0);
		i++;
    }
    return (1);
}

//cette fonction permet de vérifier si les arguments du programme sont correct
//S'ils sont corrects alors on retourne 1, sinon on retourne 0
 //Si on entre alors l'input du programme respecte le format
        // cette condition n'est valable que jusqu'à 9 ( soit pour les chiffres , le nombre supérieure ou égale à 10 ne fonctionneront pas)
int ft_check_arg(int c, char **av)
{
	int	len;
	int n;

    if (c != 2 )
        return (0); 
	len = ft_strlen(av[1]);
	if (len == 0 || (len + 1) % 8 != 0)
		return (0);
	n = (len + 1) / 8;
	if (!ft_is_str_valid(av[1], n))
		return (0);
	if (ft_nbrchar(av[1], '1') < 2)
		return (0);
	if (!ft_check_pair(av[1], n))
		return (0);
	return (n);
}

int ft_nbr_of_view(char *raw, int size)
{
    char max;
    int i;
    int nbview;

    nbview = 1;
    i = 0;
    max = raw[0];
	write(1, raw, size);
	write(1, "/ ", 2);
    while (i + 1 < size)
    {
        if ( raw[i] < raw[i+1] && max < raw[i+1])
        {
            nbview++;
            max = raw[i+1];
        }
        i++;
    }
	write (1, &nbview, 4);
	free(raw);
    return (nbview);

}

int *ft_parse(char *c, int sizemap)
{
    int i;
    int *valview;
    
    valview = 0,
    valview = malloc(4*sizemap*4);
    i = 0;
    while (*c != '\0')
    {
        valview[i] = *c - '0';
		++c;
		if (*c == ' ')
			++c;
        i++;
    }
    return (valview);
}

void	ft_set_value(char **grid, int n, int index, int digit)
{
	int	i;

	i = 1;
	grid[index][0] = digit + '0';
	while (i < n)
	{
		grid[index][i] = '\0';
		i++;
	}
}

void	ft_set_column(char **grid, int n, int j, int reverse)
{
	int	i;

	if (reverse)
	{
		i = n - 1;
		while(i >= 0)
		{
			ft_set_value(grid, n, j + i * n, n - i);
			i--;
		}
	} 
	else
	{
		i = 0;
		while(i < n)
		{
			ft_set_value(grid, n, j + i * n, i + 1);
			i++;
		}
	}
}

char*	ft_get_column(char **grid, int n, int j, int reverse)
{
	int	i;
	char *column;

	column = malloc(n);
	if (reverse)
	{
		i = n - 1;
		while(i >= 0)
		{
			column[n - 1 - i] = grid[j + i * n][0];
			i--;
		}
	} 
	else
	{
		i = 0;
		while(i < n)
		{
			column[i] = grid[j + i * n][0];
			i++;
		}
	}
	return (column);
}

char*	ft_get_row(char **grid, int n, int i, int reverse)
{
	int	j;
	int line_start;
	char *row;

	row = malloc(n);
	line_start = i * n;
	if (reverse)
	{
		j = n - 1;
		while(j >= 0)
		{
			row[n - 1 - j] = grid[line_start + j][0];
			j--;
		}
	} 
	else
	{
		j = 0;
		while(j < n)
		{
			row[j] = grid[line_start + j][0];
			j++;
		}
	}
	return (row);
}

void	ft_set_row(char **grid, int n, int i, int reverse)
{
	int	j;

	if (reverse)
	{
		j = n - 1;
		while(j >= 0)
		{
			ft_set_value(grid, n, j + i * n, n - j);
			j--;
		}
	} 
	else
	{
		j = 0;
		while(j < n)
		{
			ft_set_value(grid, n, j + i * n, j + 1);
			j++;
		}
	}
}

char	**ft_copy_grid(char **grid, int n)
{
	int 	i;
	int		j;
	char	**grid_copy;

	grid_copy = malloc(n * n * 8);
	i = 0;
	while (i < n * n)
	{
		grid_copy[i] = malloc(n + 1);
		j = 0;
		while (j <= n)
		{
			grid_copy[i][j] = grid[i][j];
			j++;
		}
		i++;
	}
	return (grid_copy);
}

void ft_free_grid(char **grid, int n)
{

	int 	i;

	i = 0;
	while (i < n * n)
	{
		free(grid[i]);
	}
	free(grid);
}

int	ft_eliminate_value(char **grid, int n, int index, int digit)
{
	int	i;
	char *c;

	i = 0;
	c = grid[index];

	while (i < n && c[i] != digit)
		i++;
	if (i == n)
		return (0);
	// c[i] == digit alors on ecrase cette valeur
	while (i < n)
	{
		c[i] = c[i + 1];
		i++;
	}
	return (1);
}

int	ft_eliminate_value_on_line(char **grid, int n, int i_digit, int j_digit)
{
	int		j;
	int 	line_start_index;
	int 	eliminated;
	char	digit;

	line_start_index = i_digit * n;
	j = 0;
	eliminated = 0;
	digit = grid[line_start_index + j_digit][0];
	while (j < j_digit)
	{
		eliminated += ft_eliminate_value(grid, n, line_start_index + j, digit);
		j++;
	}
	j++;
	while (j < n)
	{
		eliminated += ft_eliminate_value(grid, n, line_start_index + j, digit);
		j++;
	}
	return (eliminated);
}

int	ft_eliminate_value_on_column(char **grid, int n, int i_digit, int j_digit)
{
	int		i;
	char	digit;
	int 	eliminated;

	i = 0;
	eliminated = 0;
	digit = grid[i_digit * n + j_digit][0];

	while (i < i_digit)
	{
		eliminated += ft_eliminate_value(grid, n, i * n + j_digit, digit);
		i++;
	}
	i++;
	while (i < n)
	{
		eliminated += ft_eliminate_value(grid, n, i * n + j_digit, digit);
		i++;
	}
	return (eliminated);
}

void	ft_diplay_grid(char **grid, int n)
{
	int	i;

	i = 0;
	while (i < n * n)
	{
		write(1, grid[i], n);
		++i;
		if (i % n == 0)
			write(1, "\n", 1);
		else
			write(1, " ", 1);
	}
	
}

char **ft_init_map(int sizemap, int *val_view)
{
	int		i;
	int 	index;
	int		j;
	char	**grid;

	i = 0;
	grid = malloc(sizemap * sizemap * 8);
	while (i < sizemap * sizemap)
	{
		grid[i] = malloc(sizemap + 1);
		j = 0;
		while (j < sizemap)
		{
			grid[i][j] = j + 1 + '0';
			j++;
		}
		grid[i][j] = '\0';
		i++;
	}
	
	index = 0;
	while (index < 4 * sizemap)
	{
		if (val_view[index] == 1)
		{
			j = index % 4;
			if(index / 4 == 0) // u
			{
				ft_set_value(grid, sizemap, j, sizemap);
				if (val_view[index + sizemap] == 2) // d == 2
				{
					ft_set_value(grid, sizemap, j + (sizemap - 1) * sizemap , sizemap - 1);
				}
				
			}
			else if(index / 4 == 1) // d
			{
				ft_set_value(grid, sizemap, j + (sizemap - 1) * sizemap, sizemap);
				if (val_view[index - sizemap] == 2) // u == 2
				{
					ft_set_value(grid, sizemap, j, sizemap - 1);
				}	
			}
			else if(index / 4 == 2) // l
			{
				ft_set_value(grid, sizemap, j * sizemap, sizemap);
				if (val_view[index + sizemap] == 2) // r == 2
				{
					ft_set_value(grid, sizemap, (sizemap - 1) + j * sizemap, sizemap - 1);
				}
			}
			else if(index / 4 == 3) // r
			{
				ft_set_value(grid, sizemap, (sizemap - 1) + j * sizemap, sizemap);
				if (val_view[index - sizemap] == 2) // l== 2
				{
					ft_set_value(grid, sizemap, j * sizemap, sizemap - 1);
				}
			}	
		}
		else if (val_view[index] == sizemap - 1)
		{
			j = index % 4;
			if(index / 4 == 0) // u
			{
				write(1, "elim1", 5);
				ft_eliminate_value(grid, sizemap, j, sizemap - 1 + '0');
			}
			else if(index / 4 == 1) // d
			{
				ft_eliminate_value(grid, sizemap, j + (sizemap - 1) * sizemap, sizemap - 1 + '0');
			}
			else if(index / 4 == 2) // l
			{
				ft_eliminate_value(grid, sizemap, j * sizemap, sizemap - 1 + '0');
			}
			else if(index / 4 == 3) // r
			{
				ft_eliminate_value(grid, sizemap, (sizemap - 1) + j * sizemap, sizemap - 1 + '0');
			}	
		}
		else if (val_view[index] == sizemap)
		{
			if(index / 4 == 0) // u
				ft_set_column(grid, sizemap, index % 4, 0);
			else if(index / 4 == 1) // d
				ft_set_column(grid, sizemap, index % 4, 1);
			else if(index / 4 == 2) // l
				ft_set_row(grid, sizemap, index % 4, 0);
			else if(index / 4 == 3) // r
				ft_set_row(grid, sizemap, index % 4, 1);
		}
		index++;
	}	
	return grid;
}

int ft_check_solution(char **grid, int n, int *val_view)
{
	int i;

	i = 0;
	// check colonnes (u,d)
	while (i < n)
	{
		if (ft_nbr_of_view(ft_get_column(grid, n, i, 0), n) != val_view[i])
			return (0);
		if (ft_nbr_of_view(ft_get_column(grid, n, i, 1), n) != val_view[i + n])
			return (0);
		i++;
	}
	// check row (l,r)
	i = 2 * n;
	while (i < 3 * n)
	{
		if (ft_nbr_of_view(ft_get_row(grid, n, i%n, 0), n) != val_view[i])
			return (0);
		if (ft_nbr_of_view(ft_get_row(grid, n, i%n, 1), n) != val_view[i + n])
			return (0);
		i++;
	}
	return (1);
}

int ft_solution_found(char **grid, int n)
{
	int i;

	i = 0;
	while (i < n * n && ft_strlen(grid[i]) == 1)
		i++;
	if (i < n * n)
		return (0);
	
	return (1);
}

int ft_remove_duplicates(char **grid, int n)
{
	int	suppr; // on n'utilise pas check solution parce qu'il peut y avoir plusieurs solutions
	int i;
	int	len;

	i = 0;
	suppr = 1;
	while (suppr)
	{
		suppr = 0;
		i = 0;
		while (i < n * n)
		{
			len = ft_strlen(grid[i]);
			if (len == 0)
				return (0);
			if (len == 1)
			{ // eliminer la valeur sur la ligne et la colonne
				suppr += ft_eliminate_value_on_line(grid, n, i/n, i%n);
				suppr += ft_eliminate_value_on_column(grid, n, i/n, i%n);
				ft_diplay_grid(grid, n);
				write(1, "\n", 1);
			}
			i++;
		}
	}
	return (1);
}

int ft_solve(char **grid, int n, int *val_view)
{
	int i;
	int j;
	int	len;
	int	result;
	char **grid_copy;

	result = 0;
	
	i = 0;
	if (!ft_remove_duplicates(grid, n))
		return (0);

	if (ft_solution_found(grid, n))
	{
		write(1, "\nfound\n", 7);
		if (ft_check_solution(grid, n, val_view))
		{
			return (1);
		}
		return (0);
	}
	// Si plusieurs solutions
	i = 0;
	while (i < n * n && !result)
	{
		len = ft_strlen(grid[i]);
		if (len == 1)
		{
			i++;
			continue;
		}
		j = 0;
		while (j < len && !result)
		{
			grid_copy = ft_copy_grid(grid, n);
			write(1, "\ntrying\n", 8);
			ft_set_value(grid_copy, n, i, grid[i][j] - '0');
			result = ft_solve(grid_copy, n, val_view);
			j++;
			//ft_free_grid(grid_copy, n);
		}
	}
	return(result);
}

int main(int ac, char **av)
{
    int sizemap;
	int *valview;
	char **grid;
	
	sizemap = ft_check_arg(ac, av);
	printf(" sizemap %d \n", sizemap);
	if (sizemap == 0)
	{
		write(1, "Error\n", 6);
		return (0);
	}
	if (sizemap == 1)
	{
		write(1, "1\n", 2);
		return(0);
	}
    valview = ft_parse(av[1], sizemap);
	printf(" valview = ");
	for (int i = 0; i < 4 * sizemap; i++)
	{
		printf("%d ", valview[i]);
	}
	printf("\n");
	grid = ft_init_map(sizemap, valview);
	ft_diplay_grid(grid, sizemap);
	//return 0;
	write(1, "\n", 1);
	ft_solve(grid, sizemap, valview);


	/*
    int test;
    int val[5] = {2 , 3, 4, 5,  1};
    test = ft_nbr_of_view(val, 5);


    printf("le nombre de vue de test est : %d \n", test);
	*/

    

}